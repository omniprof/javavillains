/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javavillainy.badcode01;

/**
 *
 * @author omni_
 */
public class BetterEquals {
    
    // Posted byu/thellamabotherer
    public static boolean isEqual( int a, int b) {
        try {
            int c = 1 / (a-b);
        } catch (ArithmeticException e) {
            return true;
        }
        return false;
    }
    
    // Posted byu/druento
    public static String appInfo(String appname)
{
	if (appname.equals(null))
	       System.out.println("Its null!");
        else 
            System.out.println("Its not null!");

        return appname;
}
    
    public static void main(String[] args) {
        
        int y = 4;
        int z = 4;
        System.out.println(y + " is equal to " + z + "? " + isEqual(y,z));   
        
        DumbBean x = new DumbBean();
        System.out.println("x = " + x);
        
        System.out.println("appInfo " + appInfo(null));
    }   
}
