/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.javavillainy.badcode01;

import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author omni_
 */
public class DumberCode {

    // Posted byu/zhanghay
    public Date getNextDay() {
        try {
            Thread.sleep(TimeUnit.DAYS.toMillis(1));
            return new Date();
        } catch (InterruptedException ex) {
            ex.printStackTrace();
            return null;
        }
    }

    //Posted byu/matjojo1000
    public void evenNumbers() {
        int i = 2;
        while (i < 100) {
            System.out.println(i);
            i++;
            i++;
        }
    }

    //Posted byu/MilkmanDan98
    public static void checkNumber() {
        String si = "0";

        while (Integer.parseInt(si) == 0 || Integer.parseInt(si) == 1
                || Integer.parseInt(si) == 2 || Integer.parseInt(si) == 3
                || Integer.parseInt(si) == 4 || Integer.parseInt(si) == 5
                || Integer.parseInt(si) == 6 || Integer.parseInt(si) == 7
                || Integer.parseInt(si) == 8 || Integer.parseInt(si) == 9) {
            System.out.println("number is less than 10");
            si = (Integer.parseInt(si) + 1) + "";   //increment
            System.out.println("si is now " + si);
        }
    }
    
    public static void main(String[] args) {
        checkNumber();
    }

}
